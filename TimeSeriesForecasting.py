#Credits to susanli2016 for the base code for time series forecasting

# Libraries
import itertools
import tkinter as tk
from tkinter import ttk

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from fbprophet import Prophet
from pylab import rcParams

# Plot Style
plt.style.use('fivethirtyeight')
matplotlib.rcParams['axes.labelsize'] = 14
matplotlib.rcParams['xtick.labelsize'] = 12
matplotlib.rcParams['ytick.labelsize'] = 12
matplotlib.rcParams['text.color'] = 'k'

rcParams['figure.figsize'] = 18, 8

# Import Data
source_data = pd.read_excel("DATE EDITED Consumables Usage History 2016-2020_5 years.xls")

# Get Unique 'Prod No'
fru_list = list(source_data['FRU_LEVEL'].unique())


class MainClass(object):

    def __init__(self):
        self.control_gui = tk.Tk()
        self.control_frame = ttk.Frame(self.control_gui)
        self.combo_fru_title = tk.Label(master=self.control_frame, width=20, height=1, highlightthickness=0,
                                        text="Select FRU Level:")
        self.combo_fru = ttk.Combobox(master=self.control_frame, state="readonly", value=fru_list)

        self.combo_product_title = tk.Label(master=self.control_frame, width=20, height=1, highlightthickness=0,
                                            text="Select Prod No:")
        self.combo_product = ttk.Combobox(master=self.control_frame, state="readonly")

        self.label_product_length = tk.Label(master=self.control_frame, width=20, height=1, highlightthickness=0)

        self.button_run = ttk.Button(master=self.control_frame, text="Run", command=self.run_button_callback)
        self.product_data = None
        self.processed_data = None
        self.y = None

    def update_product_list(self, event):
        fru_level = self.combo_fru.get()

        if fru_level == 'NaN':
            subset_source_data = source_data[pd.isna(source_data['FRU_LEVEL'])]
        else:
            subset_source_data = source_data[source_data['FRU_LEVEL'].astype(str) == fru_level]

        product_list = list(subset_source_data['Prod No'].unique())
        self.combo_product['values'] = product_list

    def get_length_of_product(self, event):
        product = self.combo_product.get()
        if product != "":
            self.product_data = source_data[source_data['Prod No'].astype(str) == product]
            self.label_product_length['text'] = len(self.product_data)
            print(len(self.product_data))

    def create_gui(self):
        self.control_gui.minsize(height=200, width=300)
        self.control_gui.title(string="Time Series Forecasting")
        self.control_frame.pack()

        self.combo_fru_title.pack(side=tk.TOP)
        self.combo_fru.pack(side=tk.TOP)
        self.combo_fru.bind("<<ComboboxSelected>>", self.update_product_list)

        self.combo_product_title.pack(side=tk.TOP)
        self.combo_product.pack(side=tk.TOP)
        self.combo_product.bind("<<ComboboxSelected>>", self.get_length_of_product)

        self.label_product_length.pack(side=tk.TOP)

        self.button_run.pack(side=tk.TOP, pady=10)
        self.control_gui.mainloop()

    def data_pre_processing(self):
        # Sum 'Qty1' by 'Order Date' and reset the DataFrame's index
        product_sum_by_date = self.product_data.groupby('Order Date')['Qty1'].sum().reset_index()

        # Set 'Order Date' as the DataFrame's index
        product_indexed_by_date = product_sum_by_date.set_index('Order Date')

        # Conform DataFrame to be indexed by date with missing data filled by 0
        # self.processed_data = product_indexed_by_date.reindex(pd.date_range(start=product_indexed_by_date.index.min(),
        #                                                                     end=product_indexed_by_date.index.max(),
        #                                                                     freq='1D'))

        # self.processed_data = self.processed_data.fillna(0)

        self.processed_data = product_indexed_by_date
        print(self.processed_data)

    def visualise_sales_time_series_data(self):
        self.y = self.processed_data['Qty1'].resample('MS').sum() #Previously was .sum() rather than .mean
        self.y.plot(figsize=(15, 6))

        plt.figure()

        self.y = self.processed_data['Qty1'].resample('MS').sum() #Previously was .sum() rather than .mean
        self.y = self.y.fillna(0)
        self.y.plot(figsize=(15, 6))

    # Decompose the time series into three distinct components: trend, seasonality, and noise
    def time_series_decomposition(self):
        print(self.y)
        decomposition = sm.tsa.seasonal_decompose(self.y, model='additive')
        decomposition.plot()

    # Time series forecasting with ARIMA
    def time_series_forecasting_arima(self):
        p = d = q = range(0, 2)
        pdq = list(itertools.product(p, d, q))
        seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]

        print('Examples of parameter combinations for Seasonal ARIMA...')
        print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[1]))
        print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[2]))
        print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[3]))
        print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[4]))

        for param in pdq:
            for param_seasonal in seasonal_pdq:
                try:
                    mod = sm.tsa.statespace.SARIMAX(self.y,
                                                    order=param,
                                                    seasonal_order=param_seasonal,
                                                    enforce_stationarity=False,
                                                    enforce_invertibility=False)

                    results = mod.fit()

                    print('ARIMA{}x{}12 - AIC:{}'.format(param, param_seasonal, results.aic))
                except:
                    continue

        mod = sm.tsa.statespace.SARIMAX(self.y,
                                        order=(1, 1, 1),
                                        seasonal_order=(1, 1, 0, 12),
                                        enforce_stationarity=False,
                                        enforce_invertibility=False)

        results = mod.fit()

        print(results.summary().tables[1])

        results.plot_diagnostics(figsize=(16, 8))

        plt.figure()
        pred = results.get_prediction(start=pd.to_datetime('2017-01-01'), dynamic=False)
        pred_ci = pred.conf_int()

        ax = self.y['2014':].plot(label='observed')
        pred.predicted_mean.plot(ax=ax, label='One-step ahead Forecast', alpha=.7, figsize=(14, 7))

        ax.fill_between(pred_ci.index,
                        pred_ci.iloc[:, 0],
                        pred_ci.iloc[:, 1], color='k', alpha=.2)

        ax.set_xlabel('Date')
        ax.set_ylabel('Product Sales')
        plt.legend()

        plt.figure()
        y_forecasted = pred.predicted_mean
        y_truth = self.y['2017-01-01':]

        # Compute the mean square error
        mse = ((y_forecasted - y_truth) ** 2).mean()
        print('The Mean Squared Error of our forecasts is {}'.format(round(mse, 2)))
        print('The Root Mean Squared Error of our forecasts is {}'.format(round(np.sqrt(mse), 2)))

        pred_uc = results.get_forecast(steps=100)
        pred_ci = pred_uc.conf_int()

        ax = self.y.plot(label='observed', figsize=(14, 7))
        pred_uc.predicted_mean.plot(ax=ax, label='Forecast')
        ax.fill_between(pred_ci.index,
                        pred_ci.iloc[:, 0],
                        pred_ci.iloc[:, 1], color='k', alpha=.25)
        ax.set_xlabel('Date')
        ax.set_ylabel('Product Sales')

        plt.legend()

    # Time Series Modeling with Prophet
    def time_series_modelling_prophet(self):
        # Reset index to have raw data without missing data
        product_without_index = self.processed_data.reset_index()

        # Label the 'index' and 'Qty1' columns to 'ds' and 'y' respectively
        product_with_labels = product_without_index.rename(columns={'Order Date': 'ds', 'Qty1': 'y'})

        # Prophet
        prophet_model = Prophet(interval_width=0.95, daily_seasonality=True)

        # Fit the model
        prophet_model.fit(product_with_labels)

        prophet_forecast = prophet_model.make_future_dataframe(periods=36, freq='MS')
        prophet_forecast = prophet_model.predict(prophet_forecast)

        prophet_model.plot(prophet_forecast, xlabel='Date', ylabel='Sales')
        plt.title('Product Sales')

        prophet_model.plot_components(prophet_forecast);

    def run_button_callback(self):
        product = self.combo_product.get()
        if product != "":
            self.product_data = source_data[source_data['Prod No'].astype(str) == product]
            print(self.product_data)
            print("valid " + product)
            self.data_pre_processing()
            self.visualise_sales_time_series_data()
            self.time_series_decomposition()
            self.time_series_forecasting_arima()
            self.time_series_modelling_prophet()
            plt.show()
        else:
            print("invalid")


my_class = MainClass()
my_class.create_gui()
